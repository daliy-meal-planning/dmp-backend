# scripts/tfidf_similarity.py
import json
import sys
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from pythainlp.tokenize import word_tokenize   

def thai_tokenizer(text):
    return word_tokenize(text, engine='newmm')

def compute_similarity(data):
    df = pd.DataFrame(data)

    tfidf = TfidfVectorizer(tokenizer=thai_tokenizer, stop_words=None)
    tfidf_matrix = tfidf.fit_transform(df['ingredients'])

    cosine_sim = cosine_similarity(tfidf_matrix, tfidf_matrix)

    return cosine_sim

def main():
    input_data = json.loads(input())

    # คำนวณความคล้ายคลึงโดยใช้ TF-IDF
    similarity_matrix = compute_similarity(input_data)

    # ส่งผลลัพธ์กลับไปให้ NestJS
    print(json.dumps(similarity_matrix.tolist()))

if __name__ == "__main__":
    main()

sys.exit(0)