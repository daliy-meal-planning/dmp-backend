import joblib
import pandas as pd
import sys
import json

# โหลดโมเดลที่บันทึกไว้
model = joblib.load('/app/scripts/calmultioutput_model.pkl')

# อ่านข้อมูลจาก Argument
data = json.loads(sys.argv[1])

model_input = pd.DataFrame(
    [[float(data['ส่วนสูง']), float(data['น้ำหนัก']), int(data['อายุ']), str(data['เพศ']), str(data['ระดับกิจกรรม']), str(data['เป้าหมาย'])]],
    columns=['ส่วนสูง (ซม.)', 'น้ำหนัก (กก.)', 'อายุ', 'เพศ', 'ระดับกิจกรรม', 'เป้าหมาย']
)

# Perform prediction
macronutrients = model.predict(model_input)

# แปลงผลลัพธ์เป็น JSON แล้วส่งออกไป
result = {
  'TDEE': macronutrients[0][0],
  'โปรตีน (กรัม)': macronutrients[0][1],
  'ไขมัน (กรัม)': macronutrients[0][2],
  'คาร์โบไฮเดรต (กรัม)': macronutrients[0][3]
}

print(json.dumps(result))

sys.exit(0)
