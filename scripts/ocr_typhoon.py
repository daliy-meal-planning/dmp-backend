import json
import easyocr
import requests
from PIL import Image
from io import BytesIO
import re
import numpy as np

def is_number(word):
    return re.fullmatch(r'\d+(\.\d+)?', word) is not None

def download_image_from_url(url):
    response = requests.get(url)
    if response.status_code == 200:
        image = Image.open(BytesIO(response.content))
        return np.array(image)  
    else:
        print(f"ไม่สามารถดาวน์โหลดรูปภาพจาก {url}")
        return None

def load_correct_words(txt_file):
    with open(txt_file, 'r', encoding='utf-8') as f:
        words = f.read().splitlines()  
    return words

def levenshtein_distance(a, b):
    if len(a) < len(b):
        return levenshtein_distance(b, a)
    if len(b) == 0:
        return len(a)

    previous_row = range(len(b) + 1)
    for i, c1 in enumerate(a):
        current_row = [i + 1]
        for j, c2 in enumerate(b):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]

def correct_ocr_result(ocr_text, correct_words):
    corrected_words = []
    ocr_words = ocr_text.split()

    for word in ocr_words:
        if is_number(word):
            corrected_words.append(word)
        elif word in correct_words:
            corrected_words.append(word)
        else:
            closest_match = min(correct_words, key=lambda w: levenshtein_distance(word, w))
            corrected_words.append(closest_match)

    return ' '.join(corrected_words)

def call_typhoon_api(corrected_text):
    mfs = """
    มีเมนูอาหารดังข้างใต้ คัดมาแค่เมนูที่อ่านออกหรือหาข้อมูลได้ ช่วยให้เมนูที่ได้เป็นในรูปแบบ JSON ตามโครงสร้างดังนี้:
    {
      "name": "ชื่อเมนู",
      "calories": "....แคลอรี่ (โปรดระบุเป็นตัวเลขที่แน่นอน ห้ามใช้ช่วงค่า)",
      "protein": "....กรัม (โปรดระบุเป็นตัวเลขที่แน่นอน ห้ามใช้ช่วงค่า)",
      "carbohydrates": "....กรัม (โปรดระบุเป็นตัวเลขที่แน่นอน ห้ามใช้ช่วงค่า)",
      "fats": "....กรัม (โปรดระบุเป็นตัวเลขที่แน่นอน ห้ามใช้ช่วงค่า)",
      "proportion": "....กรัม (หมายถึงน้ำหนักอาหารรวมทั้งหมดในจาน, ไม่ใช่สัดส่วน โปรดระบุเป็นตัวเลขที่แน่นอน)",
      "ingredients": ["วัตถุดิบ..."]
    }
    คำตอบจะต้องอยู่ในรูปแบบ JSON และต้องมีข้อมูลตามที่กล่าวถึงข้างต้น ห้ามประมาณค่าหรือใช้ช่วงค่า
    เช่น '300' แทนที่จะเป็น '300-400'||
    """
    mfs += corrected_text


    url = "https://api.opentyphoon.ai/v1/chat/completions"
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer sk-TwLHpTGm3JpFGmCNSOPmMYbf0QNQik6mRpy4paVrh1MHJEub"
    }
    data = {
        "model": "typhoon-v1.5x-70b-instruct",
        "messages": [
            {"role": "system", "content": "You are a nutritionist expert in Thai cuisine. Answer only in Thai."},
            {"role": "user", "content": mfs}
        ],
        "max_tokens": 2000,
        "temperature": 0.7
    }

    try:
        response = requests.post(url, headers=headers, json=data)
        response.raise_for_status()

        result = response.json()
        content = result['choices'][0]['message']['content']

        # Try to extract JSON from the content
        try:
            json_start = content.index('{')
            json_end = content.rindex('}') + 1
            json_str = content[json_start:json_end]
            json_data = json.loads(json_str)
            return json.dumps(json_data, ensure_ascii=False)  # Return formatted JSON string
        except (ValueError, json.JSONDecodeError):
            print("Failed to extract valid JSON from the response")
            return None

    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except requests.exceptions.RequestException as err:
        print(f"Error occurred: {err}")
    return None

def process_image_and_call_api(url):
    txt_file = '/app/scripts/spelling_dict.txt'

    image = download_image_from_url(url)
    
    if image is not None:
        reader = easyocr.Reader(['th'], gpu=True, verbose=False)
        correct_words = load_correct_words(txt_file)
        result = reader.readtext(image, detail=0)
        ocr_text = ' '.join(result)
        corrected_text = correct_ocr_result(ocr_text, correct_words)
        api_response = call_typhoon_api(corrected_text)
        return api_response
    return None

if __name__ == "__main__":
    import sys
    url = sys.argv[1]
    result = process_image_and_call_api(url)
    if result:
        print(result)  # This will print a valid JSON string
    else:
        print("Failed to process image or get valid JSON response")
