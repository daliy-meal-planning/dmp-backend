import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { DataSource } from 'typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as admin from 'firebase-admin';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { MacrosModule } from './macros/macros.module';
import { Macro } from './macros/entities/macro.entity';
import { FoodsModule } from './foods/foods.module';
import { Food } from './foods/entities/food.entity';
import { AuthModule } from './auth/auth.module';
import { MealsModule } from './meals/meals.module';
import { Meal } from './meals/entities/meal.entity';
import { NotificationsModule } from './notifications/notifications.module';
import { MealsUsersFoodsModule } from './meals_users_foods/meals_users_foods.module';
import { Notification } from './notifications/entities/notification.entity';
import { MealsUsersFood } from './meals_users_foods/entities/meals_users_food.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { join } from 'path';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ envFilePath: ['.env.local'], isGlobal: true }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [User, Macro, Food, Notification, Meal, MealsUsersFood],
      synchronize: true,
      logging: true,
    }),
    UsersModule,
    MacrosModule,
    FoodsModule,
    AuthModule,
    MealsModule,
    NotificationsModule,
    MealsUsersFoodsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {
    // เริ่มต้น Firebase Admin SDK
    admin.initializeApp({
      credential: admin.credential.cert(
        join(
          __dirname,
          '..',
          'config',
          'dmp-project-ace0c-firebase-adminsdk-ypws9-61181727d1.json',
        ),
      ),
      storageBucket: 'gs://dmp-project-ace0c.appspot.com',
    });

    // Debugging output
    console.log('Firebase Admin SDK initialized');
    console.log('Database Host:', process.env.DATABASE_HOST);
    console.log('Database Port:', process.env.DATABASE_PORT);
    console.log('Database Username:', process.env.DATABASE_USERNAME);
    console.log('Database Name:', process.env.DATABASE_NAME);
  }
}
