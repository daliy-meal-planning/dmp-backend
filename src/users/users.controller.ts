import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  UseGuards,
  Request,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadsService } from './uploads.service';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly uploadsService: UploadsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return this.usersService.findOne(req.user.userId);
  }

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.findOne(+id);
  }

  @Patch(':id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file?: Express.Multer.File,
  ) {
    return this.usersService.update(id, updateUserDto, file);
  }

  @Patch(':id/favoriteFoods/add')
  addFavoriteFoods(
    @Param('id', ParseIntPipe) id: number,
    @Body('foodIds') foodIds: number[],
  ) {
    return this.usersService.addFavoriteFoods(id, foodIds);
  }

  @Patch(':id/favoriteFoods/remove')
  removeFavoriteFoods(
    @Param('id', ParseIntPipe) id: number,
    @Body('foodIds') foodIds: number[],
  ) {
    return this.usersService.removeFavoriteFoods(id, foodIds);
  }

  @Patch(':id/nonFavoriteFoods/add')
  addNonFavoriteFoods(
    @Param('id', ParseIntPipe) id: number,
    @Body('foodIds') foodIds: number[],
  ) {
    return this.usersService.addNonFavoriteFoods(id, foodIds);
  }

  @Patch(':id/nonFavoriteFoods/remove')
  removeNonFavoriteFoods(
    @Param('id', ParseIntPipe) id: number,
    @Body('foodIds') foodIds: number[],
  ) {
    return this.usersService.removeNonFavoriteFoods(id, foodIds);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.remove(+id);
  }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadImage(@UploadedFile() file: Express.Multer.File) {
    const imageUrl = await this.uploadsService.uploadImage(file);
    return { imageUrl };
  }

  @Post('/menu/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadMenuImage(@UploadedFile() file: Express.Multer.File) {
    const imageUrl = await this.uploadsService.uploadMenuImage(file);
    return { imageUrl };
  }
}
