/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { In, Repository } from 'typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { UploadsService } from './uploads.service';
import { MealsService } from 'src/meals/meals.service';
import { MacrosService } from 'src/macros/macros.service';
import { NotificationsService } from 'src/notifications/notifications.service';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Food)
    private foodRepository: Repository<Food>,
    private readonly uploadsService: UploadsService,
    private readonly mealsService: MealsService,
    private readonly macroservice: MacrosService,
    private readonly notificationService: NotificationsService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const {
      name,
      email,
      password,
      height,
      weight,
      age,
      gender,
      image,
      activityLevel,
      goal,
      allergies,
      favoriteFoods,
      nonFavoriteFoods,
    } = createUserDto;

    const user = new User();
    user.name = name;
    user.email = email;
    user.password = password;
    user.height = height;
    user.weight = weight;
    user.age = age;
    user.gender = gender;
    user.image =
      image ||
      'https://drive.google.com/uc?export=view&id=1Lc0bUaQRVzRrhG1XNeVZ5M3p3_pdtfa4';
    user.activityLevel = activityLevel;
    user.goal = goal;
    user.allergies = allergies;

    if (favoriteFoods && favoriteFoods.length > 0) {
      user.favoriteFoods = await this.foodRepository.find({
        where: { id: In(favoriteFoods) },
      });
    }

    if (nonFavoriteFoods && nonFavoriteFoods.length > 0) {
      user.nonFavoriteFoods = await this.foodRepository.find({
        where: { id: In(nonFavoriteFoods) },
      });
    }
    const newUser = await this.userRepository.save(user);

    await this.macroservice.calAndSaveMacro(newUser.id);
    await this.mealsService.recommendMeals(newUser.id);
    await this.notificationService.createUserNotifications(newUser.id);
    return newUser;
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  // Method สำหรับค้นหา user โดยใช้ email (ใช้ใน AuthService)
  async findOneUser(email: string): Promise<User | undefined> {
    return await this.userRepository.findOneBy({ email });
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ['favoriteFoods', 'nonFavoriteFoods'],
    });
    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }
    return user;
  }

  async update(
    id: number,
    updateUserDto: UpdateUserDto,
    file?: Express.Multer.File,
  ): Promise<User> {
    const user = await this.findOne(id);

    if (file) {
      const imageUrl = await this.uploadsService.uploadImage(file);
      user.image = imageUrl;
    }

    if (updateUserDto.favoriteFoods) {
      user.favoriteFoods = await this.foodRepository.find({
        where: { id: In(updateUserDto.favoriteFoods) },
      });
    }

    if (updateUserDto.nonFavoriteFoods) {
      user.nonFavoriteFoods = await this.foodRepository.find({
        where: { id: In(updateUserDto.nonFavoriteFoods) },
      });
    }
    Object.assign(user, updateUserDto);
    return await this.userRepository.save(user);
  }

  async remove(id: number): Promise<void> {
    const result = await this.userRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`User ${id} not found`);
    }
  }

  async addFavoriteFoods(userId: number, foodIds: number[]): Promise<User> {
    const user = await this.findOne(userId);
    if (!user.favoriteFoods) {
      user.favoriteFoods = [];
    }
    const foodsToAdd = await this.foodRepository.find({
      where: { id: In(foodIds) },
    });
    user.favoriteFoods = [...user.favoriteFoods, ...foodsToAdd];
    return await this.userRepository.save(user);
  }

  async addNonFavoriteFoods(userId: number, foodIds: number[]): Promise<User> {
    const user = await this.findOne(userId);
    if (!user.nonFavoriteFoods) {
      user.favoriteFoods = [];
    }
    const foodsToAdd = await this.foodRepository.find({
      where: { id: In(foodIds) },
    });
    user.nonFavoriteFoods = [...user.nonFavoriteFoods, ...foodsToAdd];
    return await this.userRepository.save(user);
  }

  async removeFavoriteFoods(userId: number, foodIds: number[]): Promise<User> {
    const user = await this.findOne(userId);
    user.favoriteFoods = user.favoriteFoods.filter(
      (food) => !foodIds.includes(food.id),
    );
    return await this.userRepository.save(user);
  }

  async removeNonFavoriteFoods(
    userId: number,
    foodIds: number[],
  ): Promise<User> {
    const user = await this.findOne(userId);
    user.nonFavoriteFoods = user.nonFavoriteFoods.filter(
      (food) => !foodIds.includes(food.id),
    );
    return await this.userRepository.save(user);
  }
}
