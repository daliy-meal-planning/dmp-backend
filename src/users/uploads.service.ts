import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';

@Injectable()
export class UploadsService {
  async uploadImage(file: Express.Multer.File): Promise<string> {
    const bucket = admin.storage().bucket();

    const fileName = `profile_images/${file.originalname}`;
    const fileUpload = bucket.file(fileName);

    await fileUpload.save(file.buffer, {
      metadata: {
        contentType: file.mimetype,
      },
      public: true,
    });

    const imageUrl = `https://storage.googleapis.com/${bucket.name}/${fileName}`;
    return imageUrl;
  }

  async uploadMenuImage(file: Express.Multer.File): Promise<string> {
    const bucket = admin.storage().bucket();

    const fileName = `menu_images/${file.originalname}`;
    const fileUpload = bucket.file(fileName);

    await fileUpload.save(file.buffer, {
      metadata: {
        contentType: file.mimetype,
      },
      public: true,
    });

    const imageUrl = `https://storage.googleapis.com/${bucket.name}/${fileName}`;
    return imageUrl;
  }
}
