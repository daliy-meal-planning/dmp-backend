import { Food } from 'src/foods/entities/food.entity';
import { Macro } from 'src/macros/entities/macro.entity';
import { Meal } from 'src/meals/entities/meal.entity';
import { MealsUsersFood } from 'src/meals_users_foods/entities/meals_users_food.entity';
import { Notification } from 'src/notifications/entities/notification.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ type: 'double' })
  height: number;

  @Column({ type: 'double' })
  weight: number;

  @Column()
  age: number;

  @Column()
  gender: string;

  @Column({ nullable: true })
  image: string;

  @Column()
  activityLevel: string;

  @Column()
  goal: string;

  @Column({ type: 'text', nullable: true })
  allergies: string;

  @OneToMany(() => Macro, (macro) => macro.user)
  macros: Macro[];

  @OneToMany(() => Notification, (notifications) => notifications.user)
  notifications: Notification[];

  @ManyToMany(() => Food, { nullable: true })
  @JoinTable({ name: 'user_favorite_foods' })
  favoriteFoods?: Food[];

  @ManyToMany(() => Food, { nullable: true })
  @JoinTable({ name: 'user_non_favorite_foods' })
  nonFavoriteFoods?: Food[];

  @OneToMany(() => Meal, (meal) => meal.user)
  meals: Meal[];

  @OneToMany(() => MealsUsersFood, (mealuserfood) => mealuserfood.user)
  meal_user_foods: MealsUsersFood[];
}
