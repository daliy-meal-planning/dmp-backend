import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Food } from 'src/foods/entities/food.entity';
import { UploadsService } from './uploads.service';
import { MealsModule } from 'src/meals/meals.module';
import { MacrosModule } from 'src/macros/macros.module';
import { NotificationsModule } from 'src/notifications/notifications.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Food]),
    MealsModule,
    MacrosModule,
    NotificationsModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, UploadsService],
  exports: [UsersService, UploadsService],
})
export class UsersModule {}
