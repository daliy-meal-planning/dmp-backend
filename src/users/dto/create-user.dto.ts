import {
  IsArray,
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsNumber({ allowNaN: false, allowInfinity: false })
  height: number;

  @IsNumber({ allowNaN: false, allowInfinity: false })
  weight: number;

  @IsInt()
  age: number;

  @IsString()
  gender: string;

  @IsOptional()
  @IsString()
  image?: string;

  @IsString()
  activityLevel: string;

  @IsString()
  goal: string;

  @IsOptional()
  @IsString()
  allergies?: string;

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  favoriteFoods?: number[];

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  nonFavoriteFoods?: number[];
}
