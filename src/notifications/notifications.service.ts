import { InjectRepository } from '@nestjs/typeorm';
import { Notification } from './entities/notification.entity';
import { In, Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { MealType } from './entities/notifications.enum';
import { Injectable } from '@nestjs/common';
import { UpdateNotificationDto } from './dto/update-notification.dto';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(Notification)
    private notificationRepository: Repository<Notification>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}
  async createUserNotifications(userId: number) {
    const user = await this.userRepository.findOne({ where: { id: userId } });

    const breakfastNotification = this.notificationRepository.create({
      mealType: MealType.breakfast,
      time: '08:00:00',
      user,
    });

    const lunchNotification = this.notificationRepository.create({
      mealType: MealType.lunch,
      time: '12:00:00',
      user,
    });

    const dinnerNotification = this.notificationRepository.create({
      mealType: MealType.dinner,
      time: '17:00:00',
      user,
    });

    await this.notificationRepository.save([
      breakfastNotification,
      lunchNotification,
      dinnerNotification,
    ]);
  }

  async updateNotification(
    userId: number,
    updateNotificationDto: UpdateNotificationDto,
  ): Promise<Notification> {
    const { mealType, time } = updateNotificationDto;

    const notification = await this.notificationRepository.findOne({
      where: { user: { id: userId }, mealType },
    });

    if (!notification) {
      throw new Error('Notification not found');
    }

    notification.time = time;
    return await this.notificationRepository.save(notification);
  }

  async getNotificationsByUserId(userId: number): Promise<Notification[]> {
    return await this.notificationRepository.find({
      where: { user: { id: userId } },
    });
  }

  async getNotificationsByUserIdAndMealTypes(
    userId: number,
    mealTypes: string,
  ): Promise<Notification[]> {
    const mealTypeArray = mealTypes.split(',') as MealType[];

    const validMealTypes = mealTypeArray.filter((type) =>
      Object.values(MealType).includes(type),
    );

    if (validMealTypes.length === 0) {
      throw new Error('Invalid meal types provided.');
    }

    return this.notificationRepository.find({
      where: {
        user: { id: userId },
        mealType: In(validMealTypes),
      },
      relations: ['user'],
    });
  }
}
