import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { MealType } from '../entities/notifications.enum';

export class CreateNotificationDto {
  @IsEnum(MealType)
  mealType: MealType;

  @IsString()
  @IsNotEmpty()
  time: string;
}
