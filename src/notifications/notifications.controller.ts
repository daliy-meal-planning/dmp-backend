import {
  Controller,
  Body,
  Patch,
  Param,
  Post,
  Get,
  Query,
} from '@nestjs/common';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { NotificationsService } from './notifications.service';
import { Notification } from './entities/notification.entity';

@Controller('notifications')
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @Get(':userId')
  async getNotificationsByUserId(
    @Param('userId') userId: string,
  ): Promise<Notification[]> {
    return this.notificationsService.getNotificationsByUserId(Number(userId));
  }

  @Get('getnotimeal/:userId')
  async getUserMeals(
    @Param('userId') userId: number,
    @Query('mealTypes') mealTypes: string,
  ) {
    if (!mealTypes) {
      throw new Error('mealTypes query parameter is required');
    }

    return this.notificationsService.getNotificationsByUserIdAndMealTypes(
      userId,
      mealTypes,
    );
  }

  @Post(':userId')
  async createUserNotifications(
    @Param('userId') userId: string,
  ): Promise<void> {
    return this.notificationsService.createUserNotifications(Number(userId));
  }

  @Patch(':userId')
  async updateNotification(
    @Param('userId') userId: string,
    @Body() updateNotificationDto: UpdateNotificationDto,
  ): Promise<Notification> {
    return this.notificationsService.updateNotification(
      Number(userId),
      updateNotificationDto,
    );
  }
}
