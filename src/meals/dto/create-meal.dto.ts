import {
  IsString,
  IsArray,
  IsNotEmpty,
  ArrayNotEmpty,
  IsInt,
} from 'class-validator';

export class CreateMealDto {
  @IsString()
  @IsNotEmpty()
  type: string;

  @IsArray()
  @ArrayNotEmpty()
  @IsInt({ each: true })
  foodIds: number[];
}
