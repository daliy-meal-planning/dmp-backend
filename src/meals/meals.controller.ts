import {
  Controller,
  Post,
  Param,
  Patch,
  Body,
  Get,
  Query,
  NotFoundException,
} from '@nestjs/common';
import { MealsService } from './meals.service';
import { Meal } from './entities/meal.entity';

@Controller('meals')
export class MealsController {
  constructor(private readonly mealsService: MealsService) {}

  @Post('recommend/:userId')
  async recommendMeals(@Param('userId') userId: number) {
    return this.mealsService.recommendMeals(userId);
  }

  @Patch('change-single/:userId')
  async changeSingleMeal(
    @Param('userId') userId: number,
    @Body('mealType') mealType: string,
    @Body('index') index: number,
  ) {
    return this.mealsService.changeSingleMeal(userId, mealType, index);
  }

  @Get('meal/:mealId')
  async getMealById(userId: number) {
    return this.mealsService.findRecentMealsByUserId(userId);
  }

  @Get('getnotimeal/:userId')
  async getUserMeals(
    @Param('userId') userId: number,
    @Query('mealTypes') mealTypes?: string,
  ): Promise<Meal[]> {
    const mealTypesArray = mealTypes ? mealTypes.split(',') : [];

    // เรียกใช้งาน service เพื่อดึงข้อมูลมื้ออาหารตาม userId และ mealTypes
    try {
      return await this.mealsService.findRecentMealsByUserIdAndMealType(
        userId,
        mealTypesArray,
      );
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  //------------------ocr------------------------------
  @Get('ocr-menu/:userId')
  async getOcrMenu(
    @Param('userId') userId: number,
    @Query('imageUrl') imageUrl: string,
  ) {
    console.log('Received userId:', userId);
    console.log('Received imageUrl:', imageUrl);
    const result = this.mealsService.ocrAndTyphoonMenu(userId, imageUrl);
    console.log('OCR result:', result);

    return result;
  }
}
