/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { User } from 'src/users/entities/user.entity';
import { PythonShell } from 'python-shell';
import { Meal } from './entities/meal.entity';
import { Macro } from 'src/macros/entities/macro.entity';
import { Cron } from '@nestjs/schedule';
import { MealsUsersFoodsService } from 'src/meals_users_foods/meals_users_foods.service';
import { FoodsService } from 'src/foods/foods.service';
import { CreateFoodDto } from 'src/foods/dto/create-food.dto';
import * as path from 'path';

@Injectable()
export class MealsService {
  constructor(
    @InjectRepository(Food)
    private foodRepository: Repository<Food>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Meal)
    private mealRepository: Repository<Meal>,
    @InjectRepository(Macro)
    private macroRepository: Repository<Macro>,
    private mealUserFoodService: MealsUsersFoodsService,
    private foodService: FoodsService,
  ) {}

  async runPythonTFIDF(foods: Food[]): Promise<number[][]> {
    return new Promise((resolve, reject) => {
      const data = foods.map((food) => ({
        name: food.name,
        ingredients: food.ingredients,
      }));

      const pythonShell = new PythonShell('./scripts/tfidf_similarity.py');

      pythonShell.send(JSON.stringify(data));
      pythonShell.on('message', (message) => {
        try {
          const similarityMatrix = JSON.parse(message);
          resolve(similarityMatrix);
        } catch (err) {
          reject(new Error('Failed to parse Python script output as JSON.'));
        }
      });

      pythonShell.end((err) => {
        if (err) reject(err);
      });
    });
  }

  async filterMealsWithTFIDF(userId: number): Promise<Food[]> {
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['favoriteFoods', 'nonFavoriteFoods'],
    });

    if (!user) {
      throw new Error('User not found');
    }

    // ดึงข้อมูล macro ของผู้ใช้
    const macro = await this.macroRepository.findOne({
      where: { user: { id: userId } },
    });

    if (!macro) {
      throw new Error('Macro not found for user');
    }

    const foods = await this.foodRepository.find();
    if (foods.length === 0) {
      console.log('Not found food');
      return [];
    }

    const similarityMatrix = await this.runPythonTFIDF(foods);
    const likedMenuIndex = user.favoriteFoods.map((food) =>
      foods.findIndex((f) => f.name === food.name),
    );
    const dislikedMenuIndex = user.nonFavoriteFoods.map((food) =>
      foods.findIndex((f) => f.name === food.name),
    );
    const allergens = user.allergies ? user.allergies.split(',') : [];

    // ขั้นตอนแรก: พยายามกรองด้วยเงื่อนไขที่ไม่เข้มงวด
    let filteredFoods = foods.filter((food, index) => {
      // ตรวจสอบวัตถุดิบที่แพ้
      const ingredients = food.ingredients.split(' ');
      const hasAllergen = allergens.some((allergen) =>
        ingredients.includes(allergen.trim()),
      );
      if (hasAllergen) return false;

      // ตรวจสอบเมนูที่ไม่ชอบด้วยความคล้ายคลึง (Cosine Similarity > 0.5)
      const isDisliked = dislikedMenuIndex.some(
        (dislikeIndex) => similarityMatrix[index][dislikeIndex] > 0.5,
      );
      if (isDisliked) return false;

      // ตรวจสอบเมนูที่ชอบด้วยความคล้ายคลึง (Cosine Similarity > 0.1)
      const isLiked = likedMenuIndex.some(
        (likeIndex) => similarityMatrix[index][likeIndex] > 0.1,
      );

      // คำนวณ macronutrients ของอาหารแต่ละรายการ
      const totalCalories = food.calories;
      const totalProtein = food.protein;
      const totalCarbs = food.carbohydrates;
      const totalFats = food.fats;

      // ตรวจสอบว่า macronutrients ของอาหารใกล้เคียงกับ macro ของผู้ใช้หรือไม่
      const isMacroMatch =
        Math.abs(totalCalories - macro.TDEE) / macro.TDEE <= 0.2 && // ยอมรับความคลาดเคลื่อน ±20%
        Math.abs(totalProtein - macro.protein) / macro.protein <= 0.2 &&
        Math.abs(totalCarbs - macro.carbs) / macro.carbs <= 0.2 &&
        Math.abs(totalFats - macro.fat) / macro.fat <= 0.2;

      return isLiked || (!isDisliked && isMacroMatch);
    });

    // ถ้าเมนูที่เหลือน้อยเกินไป ขยายช่วงเงื่อนไขให้กว้างขึ้น
    if (filteredFoods.length < 5) {
      filteredFoods = foods.filter((food, index) => {
        const ingredients = food.ingredients.split(' ');
        const hasAllergen = allergens.some((allergen) =>
          ingredients.includes(allergen.trim()),
        );
        if (hasAllergen) return false;

        const isDisliked = dislikedMenuIndex.some(
          (dislikeIndex) => similarityMatrix[index][dislikeIndex] > 0.3,
        );
        if (isDisliked) return false;

        const isLiked = likedMenuIndex.some(
          (likeIndex) => similarityMatrix[index][likeIndex] > 0,
        );

        // คำนวณ macronutrients อีกครั้งในกรณีที่เมนูเหลือน้อยเกินไป
        const totalCalories = food.calories;
        const totalProtein = food.protein;
        const totalCarbs = food.carbohydrates;
        const totalFats = food.fats;

        const isMacroMatch =
          Math.abs(totalCalories - macro.TDEE) / macro.TDEE <= 0.3 && // ยอมรับความคลาดเคลื่อน ±30%
          Math.abs(totalProtein - macro.protein) / macro.protein <= 0.3 &&
          Math.abs(totalCarbs - macro.carbs) / macro.carbs <= 0.3 &&
          Math.abs(totalFats - macro.fat) / macro.fat <= 0.3;

        return isLiked || (!isDisliked && isMacroMatch);
      });
    }

    // เหลือน้อยเกินไปอีก ให้คืนค่าอาหารที่ไม่มีวัตถุดิบที่แพ้
    if (filteredFoods.length < 3) {
      filteredFoods = foods.filter((food) => {
        const ingredients = food.ingredients.split(' ');
        return !allergens.some((allergen) =>
          ingredients.includes(allergen.trim()),
        );
      });
    }

    // สุ่มเรียงอาหารที่เหลือ
    filteredFoods = filteredFoods.sort(() => Math.random() - 0.5);

    console.log('Filtered Foods with Macros:', filteredFoods);
    return filteredFoods;
  }

  async filterMealsByUserPreferences(user: User): Promise<Food[]> {
    const foods = await this.foodRepository.find();
    const filteredFoods = foods.filter((food) => {
      const isNonFavorite = user.nonFavoriteFoods.some(
        (nonFavFood) => nonFavFood.id === food.id,
      );

      const allergens = user.allergies ? user.allergies.split(',') : [];
      const ingredients = food.ingredients.split(' ');
      const hasAllergen = allergens.some((allergen) =>
        ingredients.includes(allergen.trim()),
      );
      return !isNonFavorite && !hasAllergen;
    });

    return filteredFoods;
  }

  async saveMeal(type: string, foods: Food[], user: User): Promise<Meal> {
    const meal = new Meal();
    meal.type = type;
    meal.foods = foods;
    meal.user = user;
    return await this.mealRepository.save(meal);
  }

  async recommendMeals(userId: number): Promise<any> {
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['favoriteFoods', 'nonFavoriteFoods', 'meals'],
    });

    if (!user) {
      throw new Error('User not found');
    }

    const macro = await this.macroRepository.findOne({
      where: { user: { id: userId } },
    });

    if (!macro) {
      throw new Error('Macro not found for the user');
    }

    // กรองอาหารตาม TFIDF และความชอบ
    let filteredFoods = await this.filterMealsWithTFIDF(userId);

    // ดึงข้อมูลมื้ออาหารล่าสุดของผู้ใช้ (ภายใน 7 วัน)
    const recentMeals =
      await this.mealUserFoodService.getRecentMealHistory(userId);
    const recentFoodIds = new Set(
      recentMeals.map((mealUserFood) => mealUserFood.food.id),
    );

    // กรองอาหารที่แนะนำไปแล้วใน 7 วันที่ผ่านมา
    filteredFoods = filteredFoods.filter((food) => !recentFoodIds.has(food.id));

    // ถ้าจำนวนอาหารที่เหลือไม่พอแนะนำใน 1 วัน ให้เติมอาหารที่แนะนำไปแล้วกลับเข้ามา
    if (filteredFoods.length < 10) {
      const additionalFoods = await this.foodRepository.find({
        where: { id: In([...recentFoodIds]) },
      });
      filteredFoods = filteredFoods.concat(additionalFoods).slice(0, 10);
    }

    // ฟังก์ชันปรับจำนวนอาหารให้ใกล้เคียงกับ macro และจำกัดจำนวนเมนูไม่เกิน 5
    const adjustMealsToFitMacro = (meals: Food[], macroTarget: any) => {
      let totalCalories = 0;
      let totalProtein = 0;
      let totalFat = 0;
      let totalCarbs = 0;

      while (
        meals.length < 5 && // จำกัดจำนวนเมนูไม่เกิน 5
        (Math.abs(totalCalories - macroTarget.TDEE) / macroTarget.TDEE > 0.05 ||
          Math.abs(totalProtein - macroTarget.protein) / macroTarget.protein >
            0.05 ||
          Math.abs(totalFat - macroTarget.fat) / macroTarget.fat > 0.05 ||
          Math.abs(totalCarbs - macroTarget.carbs) / macroTarget.carbs > 0.05)
      ) {
        const randomFood =
          filteredFoods[Math.floor(Math.random() * filteredFoods.length)];

        // ตรวจสอบว่าอาหารนี้มีอยู่ใน meals แล้วหรือไม่เพื่อป้องกันการซ้ำ
        if (!meals.some((food) => food.id === randomFood.id)) {
          totalCalories += randomFood.calories;
          totalProtein += randomFood.protein;
          totalFat += randomFood.fats;
          totalCarbs += randomFood.carbohydrates;
          meals.push(randomFood);
        }
      }

      return {
        meals,
        totalCalories,
        totalProtein,
        totalFat,
        totalCarbs,
      };
    };

    // แบ่งอาหารออกเป็น 3 มื้อ (เช้า, กลางวัน, เย็น) และปรับตาม macro
    let breakfastMeals = adjustMealsToFitMacro([], {
      TDEE: macro.TDEE * 0.3,
      protein: macro.protein * 0.3,
      fat: macro.fat * 0.3,
      carbs: macro.carbs * 0.3,
    });

    let lunchMeals = adjustMealsToFitMacro([], {
      TDEE: macro.TDEE * 0.35,
      protein: macro.protein * 0.35,
      fat: macro.fat * 0.35,
      carbs: macro.carbs * 0.35,
    });

    let dinnerMeals = adjustMealsToFitMacro([], {
      TDEE: macro.TDEE * 0.35,
      protein: macro.protein * 0.35,
      fat: macro.fat * 0.35,
      carbs: macro.carbs * 0.35,
    });

    const meals = {
      breakfast: breakfastMeals.meals,
      lunch: lunchMeals.meals,
      dinner: dinnerMeals.meals,
    };

    // คำนวณค่าทางโภชนาการรวมสำหรับทั้ง 3 มื้อ
    const totalNutrition = {
      total_calories:
        breakfastMeals.totalCalories +
        lunchMeals.totalCalories +
        dinnerMeals.totalCalories,

      total_protein:
        breakfastMeals.totalProtein +
        lunchMeals.totalProtein +
        dinnerMeals.totalProtein,

      total_fat:
        breakfastMeals.totalFat + lunchMeals.totalFat + dinnerMeals.totalFat,

      total_carbs:
        breakfastMeals.totalCarbs +
        lunchMeals.totalCarbs +
        dinnerMeals.totalCarbs,
    };

    // บันทึกมื้ออาหารเข้าในฐานข้อมูล
    const breakfastMeal = await this.saveMeal(
      'breakfast',
      meals.breakfast,
      user,
    );
    const lunchMeal = await this.saveMeal('lunch', meals.lunch, user);
    const dinnerMeal = await this.saveMeal('dinner', meals.dinner, user);

    // บันทึกประวัติอาหารที่ถูกแนะนำไป
    await this.mealUserFoodService.saveMealHistory(
      userId,
      breakfastMeal.id,
      meals.breakfast,
    );
    await this.mealUserFoodService.saveMealHistory(
      userId,
      lunchMeal.id,
      meals.lunch,
    );
    await this.mealUserFoodService.saveMealHistory(
      userId,
      dinnerMeal.id,
      meals.dinner,
    );
    // ส่งผลลัพธ์กลับไป
    return {
      meals,
      totalNutrition,
    };
  }

  @Cron('0 0 0 * * *')
  async generateDailyMeals() {
    const users = await this.userRepository.find();
    for (const user of users) {
      await this.recommendMeals(user.id);
    }
    console.log('Generated daily meals for all users');
  }

  async changeSingleMeal(
    userId: number,
    mealType: string,
    index: number,
  ): Promise<Meal> {
    try {
      // ดึงข้อมูลผู้ใช้
      const user = await this.userRepository.findOne({
        where: { id: userId },
        relations: ['favoriteFoods', 'nonFavoriteFoods', 'meals'],
      });
      if (!user) {
        throw new Error('User not found');
      }
      // ดึงมื้ออาหาร 3 มื้อล่าสุดที่ตรงกับ mealType (เช่น lunch, dinner, breakfast)
      const recentMeals = await this.mealRepository.find({
        where: { user: { id: userId }, type: mealType.toLowerCase() }, // mealType เป็น lowercase เพื่อให้ match ถูกต้อง
        order: { createdAt: 'DESC' }, // เรียงตามเวลาที่สร้าง
        take: 5, // รับแค่ 3 รายการล่าสุด
        relations: ['foods'], // เชื่อมตาราง foods
      });
      console.log('Fetched Meals:', recentMeals);
      // ตรวจสอบว่าเจอ meal ที่ต้องการหรือไม่
      if (recentMeals.length === 0) {
        throw new Error(`${mealType} not found for the user`);
      }
      // ใช้ meal ล่าสุดที่เจอ
      const meal = recentMeals[0];
      // ตรวจสอบว่าดัชนีของอาหารถูกต้อง
      if (index < 0 || index >= meal.foods.length) {
        throw new Error('Invalid index for the food to replace');
      }
      // ดึงข้อมูล macro ของผู้ใช้
      const macro = await this.macroRepository.findOne({
        where: { user: { id: userId } },
      });
      if (!macro) {
        throw new Error('Macro not found for the user');
      }
      // อาหารที่ต้องการเปลี่ยน
      const currentFood = meal.foods[index];
      // คัดกรองอาหารตามความชอบและข้อจำกัดของผู้ใช้
      const filteredFoods = await this.filterMealsByUserPreferences(user);
      if (filteredFoods.length === 0) {
        throw new Error('No suitable food found for replacement');
      }
      // เลือกอาหารที่ใกล้เคียงกับ calorie ของอาหารปัจจุบัน
      const closestFood = filteredFoods.reduce((prev, curr) => {
        const prevCalDiff = Math.abs(prev.calories - currentFood.calories);
        const currCalDiff = Math.abs(curr.calories - currentFood.calories);
        const isAlreadyInMeal = meal.foods.some((food) => food.id === curr.id);
        return currCalDiff < prevCalDiff && !isAlreadyInMeal ? curr : prev;
      });
      // อัปเดต macronutrients ของมื้ออาหาร
      const updatedCalories = meal.foods.reduce((sum, food, i) => {
        return i === index ? sum + closestFood.calories : sum + food.calories;
      }, 0);
      const updatedProtein = meal.foods.reduce((sum, food, i) => {
        return i === index ? sum + closestFood.protein : sum + food.protein;
      }, 0);
      const updatedCarbs = meal.foods.reduce((sum, food, i) => {
        return i === index
          ? sum + closestFood.carbohydrates
          : sum + food.carbohydrates;
      }, 0);
      const updatedFats = meal.foods.reduce((sum, food, i) => {
        return i === index ? sum + closestFood.fats : sum + food.fats;
      }, 0);
      // ตรวจสอบว่า macro ที่อัปเดตแล้วใกล้เคียงกับค่า macro ของผู้ใช้หรือไม่
      if (
        Math.abs(updatedCalories - macro.TDEE) / macro.TDEE <= 0.01 &&
        Math.abs(updatedProtein - macro.protein) / macro.protein <= 0.01 &&
        Math.abs(updatedCarbs - macro.carbs) / macro.carbs <= 0.01 &&
        Math.abs(updatedFats - macro.fat) / macro.fat <= 0.01
      ) {
        // ถ้าใกล้เคียง ให้บันทึกอาหารใหม่ลงไป
        meal.foods[index] = closestFood;
        return this.mealRepository.save(meal);
      } else {
        meal.foods[index] = closestFood;
        return this.mealRepository.save(meal);
      }
    } catch (error) {
      throw error;
    }
  }

  async findRecentMealsByUserId(userId: number): Promise<Meal[]> {
    const meals = await this.mealRepository.find({
      where: {
        user: { id: userId },
        type: In(['breakfast', 'lunch', 'dinner']),
      },
      order: { createdAt: 'DESC' },
      take: 3,
      relations: ['user', 'foods'],
    });
    if (meals.length === 0) {
      throw new NotFoundException(`No meals found for user ${userId}`);
    }
    const mealOrder = {
      breakfast: 1,
      lunch: 2,
      dinner: 3,
    };
    // จัดเรียงข้อมูลตามประเภทของมื้ออาหาร
    const sortedMeals = meals.sort((a, b) => {
      return mealOrder[a.type] - mealOrder[b.type];
    });
    return sortedMeals;
  }

  async findRecentMealsByUserIdAndMealType(
    userId: number,
    mealTypes: string[],
  ): Promise<Meal[]> {
    const types =
      mealTypes && mealTypes.length > 0
        ? mealTypes
        : ['breakfast', 'lunch', 'dinner'];

    // ดึงข้อมูลจากฐานข้อมูล โดยกรองตาม userId และ mealTypes
    const meals = await this.mealRepository.find({
      where: {
        user: { id: userId },
        type: In(types),
      },
      relations: ['user', 'foods'],
    });

    if (meals.length === 0) {
      throw new NotFoundException(`No meals found for user ${userId}`);
    }

    // จัดเรียงมื้ออาหารตามลำดับ (เช่น breakfast, lunch, dinner)
    const mealOrder = {
      breakfast: 1,
      lunch: 2,
      dinner: 3,
    };

    const sortedMeals = meals.sort((a, b) => {
      return mealOrder[a.type] - mealOrder[b.type];
    });

    // ส่งคืนรายการแรกของแต่ละมื้อ (breakfast, lunch, dinner)
    return sortedMeals.slice(0, 1); // ดึงเฉพาะรายการแรก
  }

  //---------------------------------------------OCR--Part--------------------------------------------------
  async runOcrAndTyphoon(url: string, maxRetries = 3): Promise<any> {
    return new Promise((resolve, reject) => {
      const pythonScriptPath = path.resolve('scripts', 'ocr_typhoon.py');

      const options = {
        args: [url],
        pythonOptions: ['-u'], // unbuffered output
      };

      let attempts = 0;

      const runScript = () => {
        attempts++;
        console.log(`Attempt ${attempts} of ${maxRetries}`);

        PythonShell.run(pythonScriptPath, options)
          .then((result) => {
            console.log('Raw result from Python script:', result);

            if (result && result.length > 0) {
              try {
                const jsonResult = JSON.parse(result[0]);
                resolve(jsonResult);
              } catch (err) {
                console.error('Error parsing JSON:', err);
                if (attempts < maxRetries) {
                  console.log('Retrying...');
                  setTimeout(runScript, 5000); // Wait 5 seconds before retrying
                } else {
                  reject(
                    new Error(
                      'Max retries reached. Invalid JSON response from Python script',
                    ),
                  );
                }
              }
            } else {
              if (attempts < maxRetries) {
                console.log('No response, retrying...');
                setTimeout(runScript, 5000);
              } else {
                reject(
                  new Error(
                    'Max retries reached. No response from Python script',
                  ),
                );
              }
            }
          })
          .catch((err) => {
            console.error('Error running Python script:', err);
            if (attempts < maxRetries) {
              console.log('Retrying...');
              setTimeout(runScript, 5000);
            } else {
              reject(err);
            }
          });
      };

      runScript();
    });
  }

  // ฟังก์ชันสำหรับเรียก OCR และ Typhoon API
  async ocrAndTyphoonMenu(userId: number, url: string) {
    try {
      const result = await this.runOcrAndTyphoon(url);
      console.log('Python script result:', result);

      return this.ocrMeals(userId, result);
    } catch (error) {
      throw new Error(`Error processing OCR and Typhoon: ${error.message}`);
    }
  }

  async filterAPIOnlyFoods(userId: number, apiData: any): Promise<Food[]> {
    // ตรวจสอบ apiData.menu ก่อน
    if (!apiData || !Array.isArray(apiData.menu)) {
      throw new Error('Invalid API data: menu is missing or not an array.');
    }
  
    // ดึงผู้ใช้และข้อมูลอาหารที่ไม่ชอบ
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['favoriteFoods', 'nonFavoriteFoods'],
    });
  
    if (!user) {
      throw new Error('filterAPIOnlyFoods, User not found');
    }
  
    const allergens = user.allergies ? user.allergies.split(',') : [];
    const dislikedFoods = user.nonFavoriteFoods.map((food) => food.name);
  
    // Query เพื่อดึงอาหารที่มีอยู่ในฐานข้อมูลโดยตรง (ไม่ดึงข้อมูลทั้งหมด)
    const foodNamesFromApi = apiData.menu.map((food) => food.name);
  
    const existingFoods = await this.foodRepository.find({
      where: { name: In(foodNamesFromApi) },
    });
  
    // สร้าง Map จากอาหารที่ดึงมาจากฐานข้อมูล
    const existingFoodsMap = new Map<string, Food>();
    existingFoods.forEach((food) => {
      existingFoodsMap.set(food.name, food);
    });
  
    const finalFoods: Food[] = [];
  
    // วนลูปตรวจสอบข้อมูลอาหารจาก API
    for (const foodItem of apiData.menu) {
      let food: Food | null = existingFoodsMap.get(foodItem.name) || null;
  
      // ถ้าไม่มีในฐานข้อมูล ให้สร้างใหม่
      if (!food) {
        const newFood = this.foodRepository.create({
          name: foodItem.name,
          calories: foodItem.calories,
          protein: foodItem.protein,
          carbohydrates: foodItem.carbohydrates,
          fats: foodItem.fats,
          proportion: foodItem.proportion,
          ingredients: foodItem.ingredients.join(', '),
          image:
            foodItem.image ||
            'https://drive.google.com/uc?export=view&id=1ijeCuB3-V5ocm_bcrSK7_LOnG1f7fCfY',
        });
  
        // บันทึกอาหารใหม่ลงฐานข้อมูล
        food = await this.foodRepository.save(newFood);
        console.log(`Saved new food: ${food.name}`);
  
        // เพิ่มอาหารใหม่ลงใน existingFoodsMap เพื่อป้องกันการบันทึกซ้ำ
        existingFoodsMap.set(foodItem.name, food);
      }
  
      // เพิ่มอาหารที่ผ่านการสร้างใหม่เข้าไปใน finalFoods
      finalFoods.push(food);
    }
  
    return finalFoods;
  }
  

  async ocrMeals(userId: number, apiData: any): Promise<any> {
    // Log ข้อมูลผู้ใช้ที่ถูกดึงมา
    console.log(`Fetching user with ID: ${userId}`);

    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['favoriteFoods', 'nonFavoriteFoods'],
    });

    if (!user) {
      throw new Error('User not found');
    }

    // Log ข้อมูลผู้ใช้ที่พบ
    console.log(`User found: ${JSON.stringify(user)}`);

    const macro = await this.macroRepository.findOne({
      where: { user: { id: userId } },
    });

    if (!macro) {
      throw new Error('Macro not found for user');
    }

    // Log ข้อมูล macro ของผู้ใช้
    console.log(`Macro found for user: ${JSON.stringify(macro)}`);

    // ดึงข้อมูลอาหารที่มาจาก API หรือจากฐานข้อมูล
    console.log(
      `Fetching final foods from API and database for user: ${userId}`,
    );
    const finalFoods = await this.filterAPIOnlyFoods(userId, apiData);

    // Log อาหารที่ได้หลังจากการกรอง
    console.log(`Final foods after filtering: ${JSON.stringify(finalFoods)}`);

    // ฟังก์ชันปรับจำนวนอาหารให้ใกล้เคียงกับ macro
    const adjustMealsToFitMacro = (meals: Food[], macroTarget: any) => {
      let totalCalories = 0;
      let totalProtein = 0;
      let totalFat = 0;
      let totalCarbs = 0;

      finalFoods.forEach((food) => {
        // คำนวณค่า macro ที่จะให้เหมาะกับ 1 มื้อ โดยคิดเป็น 35%
        const newTotalCalories = totalCalories + food.calories;
        const newTotalProtein = totalProtein + food.protein;
        const newTotalFat = totalFat + food.fats;
        const newTotalCarbs = totalCarbs + food.carbohydrates;

        // เช็คว่าอาหารนี้จะทำให้เกินเป้าหมาย macro หรือไม่ ถ้าเกินให้ข้ามไป
        if (
          newTotalCalories <= macroTarget.TDEE && // ปรับค่า TDEE เป็น 35% (คิดเป็นมื้อ) ต้องใส่ *0.35
          newTotalProtein <= macroTarget.protein &&
          newTotalFat <= macroTarget.fat &&
          newTotalCarbs <= macroTarget.carbs
        ) {
          console.log(
            `Target macro for one meal: TDEE: ${macroTarget.TDEE}, Protein: ${macroTarget.protein}, Fat: ${macroTarget.fat}, Carbs: ${macroTarget.carbs}`,
          );

          // ถ้าไม่เกินเป้าหมาย, เพิ่มอาหารนี้เข้าไปในมื้ออาหาร
          if (!meals.some((meal) => meal.id === food.id)) {
            totalCalories = newTotalCalories;
            totalProtein = newTotalProtein;
            totalFat = newTotalFat;
            totalCarbs = newTotalCarbs;
            meals.push(food);
          }
        }
      });

      // Log ค่าทางโภชนาการสุดท้ายที่ได้
      console.log(`Final adjusted meals: ${JSON.stringify(meals)}`);
      console.log(
        `Total calories: ${totalCalories}, Total protein: ${totalProtein}, Total fat: ${totalFat}, Total carbs: ${totalCarbs}`,
      );

      return {
        meals,
        totalCalories,
        totalProtein,
        totalFat,
        totalCarbs,
      };
    };

    // ปรับเมนูตาม macro โดยไม่แยกมื้อ
    const mealsData = adjustMealsToFitMacro([], {
      TDEE: macro.TDEE,
      protein: macro.protein,
      fat: macro.fat,
      carbs: macro.carbs,
    });

    // Log เมนูที่ถูกสร้างขึ้นพร้อมค่าทางโภชนาการรวม
    console.log(`Final meals data: ${JSON.stringify(mealsData)}`);

    // ส่งข้อมูลกลับในรูปแบบ JSON
    return {
      meals: mealsData.meals,
      totalNutrition: {
        total_calories: mealsData.totalCalories,
        total_protein: mealsData.totalProtein,
        total_fat: mealsData.totalFat,
        total_carbs: mealsData.totalCarbs,
      },
    };
  }
}
