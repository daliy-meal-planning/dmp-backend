import { Module } from '@nestjs/common';
import { MealsService } from './meals.service';
import { MealsController } from './meals.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { User } from 'src/users/entities/user.entity';
import { Meal } from './entities/meal.entity';
import { Macro } from 'src/macros/entities/macro.entity';
import { MealsUsersFoodsModule } from 'src/meals_users_foods/meals_users_foods.module';
import { FoodsModule } from 'src/foods/foods.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Meal, Food, User, Macro]),
    MealsUsersFoodsModule,
    FoodsModule,
  ],
  controllers: [MealsController],
  providers: [MealsService],
  exports: [MealsService],
})
export class MealsModule {}
