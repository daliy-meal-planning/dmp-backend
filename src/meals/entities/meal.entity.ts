import { Food } from 'src/foods/entities/food.entity';
import { MealsUsersFood } from 'src/meals_users_foods/entities/meals_users_food.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Meal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string;

  @ManyToMany(() => Food)
  @JoinTable({ name: 'meal_foods' })
  foods: Food[];

  @OneToMany(() => MealsUsersFood, (meal_user_food) => meal_user_food.meal)
  meal_user_foods: MealsUsersFood[];

  @ManyToOne(() => User, (user) => user.meals)
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
