import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class FoodsService {
  constructor(
    @InjectRepository(Food)
    private foodRepository: Repository<Food>,
  ) {}

  async create(createFoodDto: CreateFoodDto): Promise<Food> {
    const foodImage =
      createFoodDto.image ||
      'https://drive.google.com/uc?export=view&id=1ijeCuB3-V5ocm_bcrSK7_LOnG1f7fCfY';
    const newFood = this.foodRepository.create({
      ...createFoodDto,
      image: foodImage,
    });

    return this.foodRepository.save(newFood);
  }

  findAll() {
    const food = this.foodRepository.find();
    return food;
  }

  async findOne(id: number) {
    const food = await this.foodRepository.findOne({ where: { id } });
    if (!food) {
      throw new NotFoundException(`Food ${id} not found`);
    }
    return food;
  }

  async update(id: number, updateFoodDto: UpdateFoodDto) {
    const food = await this.findOne(id);
    Object.assign(food, updateFoodDto);
    return await this.foodRepository.save(food);
  }

  async remove(id: number) {
    const result = await this.foodRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`User ${id} not found`);
    }
  }

  async findByName(name: string): Promise<Food[]> {
    console.log(name);
    const foods = await this.foodRepository.find({
      where: {
        name: Like(`%${name}%`),
      },
    });
    if (!foods.length) {
      throw new NotFoundException(
        `No food found with name containing: ${name}`,
      );
    }
    return foods;
  }
}
