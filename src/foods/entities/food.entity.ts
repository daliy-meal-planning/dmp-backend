import { Meal } from 'src/meals/entities/meal.entity';
import { MealsUsersFood } from 'src/meals_users_foods/entities/meals_users_food.entity';
import {
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Food {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ type: 'double' })
  calories: number;

  @Column({ type: 'double' })
  protein: number;

  @Column({ type: 'double' })
  carbohydrates: number;

  @Column({ type: 'double' })
  fats: number;

  @Column({ type: 'double' })
  proportion: number;

  @Column()
  ingredients: string;

  @Column({ nullable: true })
  image: string;

  @OneToMany(() => MealsUsersFood, (meal_user_food) => meal_user_food.meal)
  meal_user_foods: MealsUsersFood[];

  @ManyToMany(() => Meal, (meal) => meal.foods)
  meals: Meal[];
}
