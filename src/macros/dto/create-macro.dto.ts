import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateMacroDto {
  @IsNumber()
  @IsNotEmpty()
  TDEE: number;

  @IsNumber()
  @IsNotEmpty()
  protein: number;

  @IsNumber()
  @IsNotEmpty()
  fat: number;

  @IsNumber()
  @IsNotEmpty()
  carbs: number;

  @IsNotEmpty()
  userId: number;
}
