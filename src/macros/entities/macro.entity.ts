import { User } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Macro {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'double' })
  TDEE: number;

  @Column({ type: 'double' })
  protein: number;

  @Column({ type: 'double' })
  fat: number;

  @Column({ type: 'double' })
  carbs: number;

  @ManyToOne(() => User, (user) => user.macros)
  user: User;
}
