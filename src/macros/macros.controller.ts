import { Controller, Get, Post, Param } from '@nestjs/common';
import { MacrosService } from './macros.service';

@Controller('macros')
export class MacrosController {
  constructor(private readonly macrosService: MacrosService) {}

  @Post(':id')
  async calAndSaveMacro(@Param('id') id: number) {
    const macro = await this.macrosService.calAndSaveMacro(id);
    return macro;
  }

  // @Post(':id')
  // async calAndSaveMacro(
  //   @Param('id') id: number,
  //   @Body() body: { userId: number },
  // ) {
  //   const userId = body.userId;
  //   if (userId !== id) {
  //     throw new Error('User ID mismatch');
  //   }
  //   const macro = await this.macrosService.calAndSaveMacro(userId);
  //   return macro;
  // }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.macrosService.findOne(+id);
  }
}
