/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import { UpdateMacroDto } from './dto/update-macro.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Macro } from './entities/macro.entity';
import { Repository } from 'typeorm';
import { PythonShell } from 'python-shell';

@Injectable()
export class MacrosService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Macro)
    private macroRepository: Repository<Macro>,
  ) {}

  async calMacronutrientWithModel(data: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const pythonShell = new PythonShell('./scripts/predict_macro.py', {
        args: [JSON.stringify(data)],
      });
      pythonShell.on('message', (message) => {
        try {
          const parsedMessage = JSON.parse(message);
          resolve(parsedMessage);
        } catch (err) {
          reject(new Error('Failed to parse Python script output as JSON.'));
        }
      });
      pythonShell.end((err) => {
        if (err) reject(err);
      });
    });
  }

  async calAndSaveMacro(id: number): Promise<Macro> {
    const user = await this.userRepository.findOne({
      where: { id: id },
    });
    if (!user) {
      throw new Error('User not found');
    }
    console.log('User Data:', user);
    const data = {
      ส่วนสูง: user.height,
      น้ำหนัก: user.weight,
      อายุ: user.age,
      เพศ: user.gender,
      ระดับกิจกรรม: user.activityLevel,
      เป้าหมาย: user.goal,
    };
    try {
      const macronutrients = await this.calMacronutrientWithModel(data);
      let macro = await this.macroRepository.findOne({
        where: { user: user },
      });
      if (macro) {
        await this.macroRepository.remove(macro);
        console.log('Deleted old macro for user:', user.id);
      }
      macro = new Macro();
      macro.TDEE = macronutrients.TDEE;
      macro.protein = macronutrients['โปรตีน (กรัม)'];
      macro.fat = macronutrients['ไขมัน (กรัม)'];
      macro.carbs = macronutrients['คาร์โบไฮเดรต (กรัม)'];
      macro.user = user;
      console.log('Data sent to Python script:', data);
      return this.macroRepository.save(macro);
    } catch (error) {
      console.error('Error calculating macronutrients:', error);
      throw new Error('Failed to calculate macronutrients');
    }
  }

  async findOne(userId: number): Promise<Macro> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) {
      throw new Error('User not found.');
    }
    return await this.macroRepository.findOne({
      where: { user: { id: userId } },
    });
  }
}
