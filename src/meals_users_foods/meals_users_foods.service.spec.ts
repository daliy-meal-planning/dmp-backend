import { Test, TestingModule } from '@nestjs/testing';
import { MealsUsersFoodsService } from './meals_users_foods.service';

describe('MealsUsersFoodsService', () => {
  let service: MealsUsersFoodsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MealsUsersFoodsService],
    }).compile();

    service = module.get<MealsUsersFoodsService>(MealsUsersFoodsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
