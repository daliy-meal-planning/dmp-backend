import { Test, TestingModule } from '@nestjs/testing';
import { MealsUsersFoodsController } from './meals_users_foods.controller';
import { MealsUsersFoodsService } from './meals_users_foods.service';

describe('MealsUsersFoodsController', () => {
  let controller: MealsUsersFoodsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MealsUsersFoodsController],
      providers: [MealsUsersFoodsService],
    }).compile();

    controller = module.get<MealsUsersFoodsController>(
      MealsUsersFoodsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
