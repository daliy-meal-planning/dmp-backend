import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Meal } from 'src/meals/entities/meal.entity';
import { Food } from 'src/foods/entities/food.entity';

@Entity('meals_users_food')
export class MealsUsersFood {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @ManyToOne(() => Meal, (meal) => meal.meal_user_foods)
  @JoinColumn({ name: 'mealId' })
  meal: Meal;

  @ManyToOne(() => Food, (food) => food.meal_user_foods)
  @JoinColumn({ name: 'foodId' })
  food: Food;

  @ManyToOne(() => User, (user) => user.meal_user_foods)
  @JoinColumn({ name: 'userId' })
  user: User;
}
