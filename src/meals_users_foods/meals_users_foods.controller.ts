import { Controller } from '@nestjs/common';
import { MealsUsersFoodsService } from './meals_users_foods.service';

@Controller('meals-users-foods')
export class MealsUsersFoodsController {
  constructor(
    private readonly mealsUsersFoodsService: MealsUsersFoodsService,
  ) {}
}
