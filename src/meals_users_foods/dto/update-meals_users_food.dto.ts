import { PartialType } from '@nestjs/swagger';
import { CreateMealsUsersFoodDto } from './create-meals_users_food.dto';

export class UpdateMealsUsersFoodDto extends PartialType(
  CreateMealsUsersFoodDto,
) {}
