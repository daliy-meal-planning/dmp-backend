import { Module } from '@nestjs/common';
import { MealsUsersFoodsService } from './meals_users_foods.service';
import { MealsUsersFoodsController } from './meals_users_foods.controller';
import { User } from 'src/users/entities/user.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Meal } from 'src/meals/entities/meal.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MealsUsersFood } from './entities/meals_users_food.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MealsUsersFood, Meal, Food, User])],
  controllers: [MealsUsersFoodsController],
  providers: [MealsUsersFoodsService],
  exports: [MealsUsersFoodsService],
})
export class MealsUsersFoodsModule {}
