import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MealsUsersFood } from './entities/meals_users_food.entity';
import { MoreThan, Repository } from 'typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { User } from 'src/users/entities/user.entity';
import { Meal } from 'src/meals/entities/meal.entity';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class MealsUsersFoodsService {
  constructor(
    @InjectRepository(MealsUsersFood)
    private mealsUsersFoodRepository: Repository<MealsUsersFood>,
    @InjectRepository(Meal)
    private mealRepository: Repository<Meal>,
    @InjectRepository(Food)
    private foodRepository: Repository<Food>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async saveMealHistory(
    userId: number,
    mealId: number,
    foods: Food[],
  ): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    const meal = await this.mealRepository.findOne({ where: { id: mealId } });
    if (!user || !meal) {
      throw new Error('user or food not found');
    }
    const date = new Date();
    for (const food of foods) {
      const mealUserFood = new MealsUsersFood();
      mealUserFood.user = user;
      mealUserFood.meal = meal;
      mealUserFood.food = food;
      mealUserFood.date = date;

      await this.mealsUsersFoodRepository.save(mealUserFood);
    }
  }

  async getRecentMealHistory(userId: number): Promise<MealsUsersFood[]> {
    const oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 14);

    return this.mealsUsersFoodRepository.find({
      where: { user: { id: userId }, date: MoreThan(oneWeekAgo) },
      relations: ['user', 'food', 'meal'],
      order: { date: 'DESC' },
    });
  }

  @Cron('0 0 1 */14 *')
  async deleteOldMealHistory(): Promise<void> {
    const oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 14);

    try {
      const users = await this.userRepository.find();

      for (const user of users) {
        await this.mealsUsersFoodRepository
          .createQueryBuilder('meals_users_food')
          .delete()
          .where('meals_users_food.userId = :userId', { userId: user.id })
          .andWhere('meals_users_food.date < :oneWeekAgo', { oneWeekAgo })
          .execute();

        console.log(`Deleted old meal history for user ${user.id}`);
      }
    } catch (error) {
      console.error(`Failed to delete meal history:`, error.message);
      throw new Error(`Unable to delete old meal history: ${error.message}`);
    }
  }
}
